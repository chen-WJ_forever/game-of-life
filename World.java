import java.util.*;
public class World {
    public final static boolean LIVE=true,DEAD=false;
    private int lx,ly;
    private int Generation;
    private Cell[][] cells=new Cell[100][100];

    public World(int lx,int ly)
    {
        this.lx=lx;
        this.ly=ly;
        Generation=0;
        for(int i=0;i<lx;i++)
        {
            for(int j=0;j<ly;j++)
            {
                cells[i][j]=new Cell(i,j);
                cells[i][j].setLive(DEAD);
            }
        }
    }

    public int getLx() {
        return lx;
    }

    public int getLy() {
        return ly;
    }

    public boolean getCell(int x,int y)
    {
        return cells[x][y].isLive();
    }

    public int getGeneration() {
        return Generation;
    }

    public void initWorld()
    {
        for(int i=0;i<lx;i++)
        {
            for(int j=0;j<ly;j++)
            {
                if(Math.random()>0.5)
                    cells[i][j].setLive(LIVE);
                else
                    cells[i][j].setLive(DEAD);
            }
        }
    }

    public void clearWorld()
    {
        for(int i=0;i<lx;i++)
        {
            for(int j=0;j<ly;j++)
            {
                cells[i][j].setLive(DEAD);
                cells[i][j].setX(-1);
                cells[i][j].setY(-1);
            }
        }
        Generation=0;
    }

    public void updateCell()
    {
        Cell[][] c=new Cell[lx][ly];
        for(int i=0;i<lx;i++)
        {
            for(int j=0;j<ly;j++)
            {
                c[i][j]=new Cell(i,j);
                int count=getLiveSum(i,j);
                if(count==3)
                    c[i][j].setLive(LIVE);
                else if(count>=4||count<=1)
                    c[i][j].setLive(DEAD);
                else
                    c[i][j].setLive(cells[i][j].isLive());
            }
        }
        for(int i=0;i<lx;i++)
        {
            for(int j = 0; j < ly; j++)
                cells[i][j]=c[i][j];

        }
        Generation++;
    }

    private int getLiveSum(int x,int y)
    {
        int count=0;
        for(int i=x-1;i<=x+1;i++)
        {
            for(int j=y-1;j<=y+1;j++)
            {
                if(i<0||i>=lx||j<0||j>=ly||(i==x&&j==y))
                    continue;
                if(cells[i][j].isLive()==LIVE)
                    count++;
            }
        }
        return count;
    }


}
