import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.*;
import java.awt.event.ActionListener;

public class GameUI extends JFrame implements ActionListener {
    int lx,ly;
    private World world;
    private JButton[][] JWorld;
    private JLabel generation_show;
    private JButton randomInitial,BeginandOver,Next,StopandContinue,Exit;
    private boolean isRunning;
    private Thread thread;

    public GameUI(String name,World world){
        super(name);
        this.lx=world.getLx();
        this.ly=world.getLy();
        this.world=world;
        initGUI();
    }
    public void initGUI(){
        JPanel backPanel,bottomPanel,centrePanel;
        backPanel=new JPanel(new BorderLayout());
        bottomPanel=new JPanel();
        centrePanel=new JPanel(new GridLayout(lx,ly));
        this.setContentPane(backPanel);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        backPanel.add(centrePanel,"Center");
        backPanel.add(bottomPanel,"South");

        JWorld=new JButton[lx][ly];
        generation_show=new JLabel("当前代数：0");
        randomInitial=new JButton("随机生成细胞");
        BeginandOver=new JButton("开始游戏");
        Next=new JButton("下一代");
        StopandContinue=new JButton("暂停游戏");
        Exit=new JButton ("退出游戏");

        for (int i =0;i<lx;i++){
            for (int j=0;j<ly;j++){
                JWorld[i][j]=new JButton("");
                JWorld[i][j].setBackground(Color.white);
                centrePanel.add(JWorld[i][j]);
            }
        }


        bottomPanel.add(randomInitial);
        bottomPanel.add(BeginandOver);
        bottomPanel.add(Next);
        bottomPanel.add(StopandContinue);
        bottomPanel.add(Exit);
        bottomPanel.add(generation_show);


        //设置窗口界面
        int sizelx,sizely;
        sizelx=Math.min((lx+1)*40,800);
        sizely=Math.min(ly*40,1500);
        sizely=Math.max(ly*40,500);



        this.setSize(sizely,sizelx);
        this.setResizable(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        //注册监听器
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                System.exit(0);
            }
        });

        randomInitial.addActionListener(this);
        StopandContinue.addActionListener(this);
        Next.addActionListener(this);
        Exit.addActionListener(this);
        BeginandOver.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource()==randomInitial&&BeginandOver.getText()=="开始游戏"){
            //随机生成第一代
            world.initWorld();
            isRunning=false;
            showworld();
            thread=null;
            randomInitial.setText("重新生成");
        }
        else if (e.getSource()==BeginandOver&&BeginandOver.getText()=="开始游戏"&&randomInitial.getText()=="重新生成"){
            isRunning=true;
            thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    while(isRunning){
                        Change();
                        try{
                            Thread.sleep(1000);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                    }

                }
            });
            thread.start();
            BeginandOver.setText("结束游戏");
        }
        else if (e.getSource()==BeginandOver &&BeginandOver.getText()=="结束游戏"){
            isRunning=false;
            thread=null;
            world.clearWorld();
            showworld();
            generation_show.setText("当前代数:0");
            BeginandOver.setText("开始游戏");
            StopandContinue.setText("暂停游戏");
            randomInitial.setText("随机生成细胞");

        }
        else if (e.getSource()==StopandContinue &&StopandContinue.getText()=="暂停游戏"){
            isRunning=false;
            thread=null;
            StopandContinue.setText("继续游戏");
        }
        else if (e.getSource()==StopandContinue &&StopandContinue.getText()=="继续游戏"){
            isRunning=true;
            thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    while(isRunning){
                        Change();
                        try{
                            Thread.sleep(1000);
                        } catch (InterruptedException interruptedException) {
                            interruptedException.printStackTrace();
                        }
                    }

                }
            });
            thread.start();
            StopandContinue.setText("暂停游戏");

        }
        else if (e.getSource()==Next &&Next.getText()=="继续游戏"){
            Change();
            isRunning=false;
            thread=null;
        }
        else if (e.getSource()==Exit){
            isRunning=false;
            thread=null;
            this.dispose();
            System.exit(0);
        }
    }
    public void Change(){
        world.updateCell();
        showworld();
        generation_show.setText("当前代数："+world.getGeneration());
    }
    public void showworld(){
        for (int i=0;i<lx;i++){
            for (int j=0;j<ly;j++){
                if (world.getCell(i,j)){
                    JWorld[i][j].setBackground(Color.green);}
                else{
                    JWorld[i][j].setBackground(Color.white);
                }
            }
        }

    }



}
